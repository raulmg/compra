#!/usr/bin/env pythoh3

'''
Programa para mostrar la lista de la compra
'''
def main(especifica=None):
    habitual = ["patatas", "leche", "pan"]
    especifica = []

    elemento = input("Elemento a comprar: ")
    while elemento != "":
        especifica.append(elemento)
        elemento = input("Elemento a comprar: ")

    lista_final = habitual.copy()

    for k in especifica:
        if k not in habitual:
            lista_final += [k]

    print("Lista de la compra: ")
    for n in lista_final:
        print(n)

    print("Elementos habituales: ", len(habitual))
    print("Elementos especificos: ", len(especifica))
    print("Elementos en lista; ", len(lista_final))

if __name__ == '__main__':
    main()
